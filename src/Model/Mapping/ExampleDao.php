<?php
namespace MyProject\Model\Mapping;

use Exception;
use MyProject\Kernel\Dao;
use MyProject\Model\Classes\Example;

class ExampleDao
{
    public static function get($id)
    {
        $dbh = Dao::open();

        $query = "SELECT * FROM `example` WHERE `id` = :id;";
        
        $sth = $dbh->prepare($query);
        $sth->bindParam(":id", $id);
        
        $res = $sth->execute();
        if (!res)
        {
            // $error = $sth->errorInfo();
            // die($error[2]);
            throw new Exception("PDO : Query error.");
        }
        
        $sth->setFetchMode(
            PDO::FETCH_CLASS,
            "MyProject\\Model\\Classes\\Example"
        );
        $items = $sth->fetchAll();

        Dao::close();

        return $items;
    }
}